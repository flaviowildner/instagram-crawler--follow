from typing import List

import requests

from inscrawler.constants.consts import ENTITY_URL, SERVICE_IDENTIFIER
from inscrawler.model.profile import Profile


def create_or_update_profile(profile: Profile):
    requests.put(f'{ENTITY_URL}/profile',
                 json=profile.__dict__)


def get_or_create_profile(username: str):
    response = requests.get(f'{ENTITY_URL}/profile/{username}')
    return response.json()


def create_usernames_batch(usernames: List[str]):
    response = requests.put(f'{ENTITY_URL}/profile/batch',
                            json=[{"username": username} for username in usernames])
    return response.json()


def get_profiles_to_crawl() -> List[str]:
    response = requests.get(f'{ENTITY_URL}/profile/get_profiles_to_crawl',
                            params={'n_profiles': 10, 'service_identifier': SERVICE_IDENTIFIER})
    if response.status_code != 200:
        raise Exception(f'Error when getting the profile list from Entity API - {response.json()}')

    return response.json()
