# HTML Classes
# Profile

PROFILE_NAME = ".rhpdm"
PROFILE_DESCRIPTION = ".-vDIg > span"
PROFILE_PUBLIC_ACCOUNT_PHOTO = "._6q-tv"
PROFILE_PRIVATE_ACCOUNT_PHOTO = ".be6sR"
PROFILE_STATISTICS = "._aa_7 > ._aa_5"
PROFILE_FOLLOWERS_ELEMENTS = "._aacl._aaco._aacw._aacx._aad7._aade"  # check if it can be changed to ".Jv7Aj.MqpiF"

FOLLOWERS_LAST_PROFILE = "._aae- > li:last-child ._aacl._aaco._aacw._aacx._aad7._aade"

# JS Scripts
FOLLOWERS_SCROLL_DOWN = "document.querySelector(\"._aano\").scrollTo(0, document.querySelector(" \
                        "\"._aano\").scrollHeight); "
