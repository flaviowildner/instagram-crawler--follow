import os

USERNAME = os.environ.get('IG_USERNAME', '')
PASSWORD = os.environ.get('IG_PASSWORD', '')

SERVICE_IDENTIFIER = 'follow'
ENTITY_URL = os.environ.get('ENTITY_URL', '')

RETRY_TIMEOUT = int(os.environ.get('RETRY_TIMEOUT', 10))
MAX_RETRY_TIMEOUT = int(os.environ.get('MAX_RETRY_TIMEOUT', 60))
