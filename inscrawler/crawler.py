from __future__ import unicode_literals

import glob
import logging
import os
import time
from builtins import open
from time import sleep
from typing import List

from .api.profile_api import create_usernames_batch
from .browser import Browser
from .constants import consts
from .constants.html_selectors import PROFILE_STATISTICS, PROFILE_FOLLOWERS_ELEMENTS, FOLLOWERS_SCROLL_DOWN, \
    FOLLOWERS_LAST_PROFILE
from .exceptions import RetryException
from .utils import retry


class Logging(object):
    PREFIX = "instagram-crawler"

    def __init__(self):
        try:
            timestamp = int(time.time())
            self.cleanup(timestamp)
            self.logger = open("/tmp/%s-%s.log" %
                               (Logging.PREFIX, timestamp), "w")
            self.log_disable = False
        except Exception:
            self.log_disable = True

    def cleanup(self, timestamp):
        days = 86400 * 7
        days_ago_log = "/tmp/%s-%s.log" % (Logging.PREFIX, timestamp - days)
        for log in glob.glob("/tmp/instagram-crawler-*.log"):
            if log < days_ago_log:
                os.remove(log)

    def log(self, msg):
        if self.log_disable:
            return

        self.logger.write(msg + "\n")
        self.logger.flush()

    def __del__(self):
        if self.log_disable:
            return
        self.logger.close()


class InsCrawler(Logging):
    URL = "https://www.instagram.com"
    RETRY_LIMIT = 10

    def __init__(self, has_screen: bool = False):
        super(InsCrawler, self).__init__()
        self.browser = Browser(has_screen)
        self.page_height = 0

    def _dismiss_login_prompt(self):
        ele_login = self.browser.find_one(".Ls00D .Szr5J")
        if ele_login:
            ele_login.click()

    def login(self):
        browser = self.browser
        url = "%s/accounts/login/" % InsCrawler.URL
        browser.get(url)
        u_input = browser.find_one('input[name="username"]')
        u_input.send_keys(consts.USERNAME)
        p_input = browser.find_one('input[name="password"]')
        p_input.send_keys(consts.PASSWORD)

        login_btn = browser.find_one(".L3NKy")
        login_btn.click()

        @retry()
        def check_login():
            if browser.find_one('input[name="username"]'):
                raise RetryException()

        check_login()

    def get_user_profile(self, username: str) -> None:
        browser = self.browser
        url = "%s/%s/" % (InsCrawler.URL, username)
        browser.get(url)

        sleep(5)

        statistics_elem = self.browser.find(PROFILE_STATISTICS)
        post_num_elem = statistics_elem[0]
        follower_elem = statistics_elem[1]
        following_elem = statistics_elem[2]

        followers = []
        followings = []

        follower_btn = follower_elem
        follower_btn.click()

        try:
            follower_elems = list(browser.find(PROFILE_FOLLOWERS_ELEMENTS, waittime=0.6) or [])

            if len(follower_elems) == 0:
                logging.info(f'Cannot craw profile \'{username}\' because it\'s private.')
                return

            logging.info(f'Scrolling follower list...')

            follower_elems[-1].location_once_scrolled_into_view
            sleep(2)

            follower_elems = list(browser.find(PROFILE_FOLLOWERS_ELEMENTS) or [])
            last_follower = follower_elems[-1]
            username_last_check = last_follower.text
            for i in range(40):
                # Scroll down
                self.browser.driver.execute_script(FOLLOWERS_SCROLL_DOWN)
                sleep(2)

                # Get last follower
                try:
                    last_follower = browser.find_one(FOLLOWERS_LAST_PROFILE)
                    current_username = last_follower.text
                except AttributeError:
                    break

                # Check if the last username of current iteration is the same as the previous iteration
                if current_username == username_last_check:
                    break

                # Save last username of list
                username_last_check = current_username

            follower_elems = list(browser.find(PROFILE_FOLLOWERS_ELEMENTS,
                                               waittime=1) or [])  # avoid assign None to list (which causes an exception)
            followers_usernames: List[str] = list([ele.text for ele in follower_elems if ele.text != ""])
            # followers: List[Profile] = [get_or_create_profile(username) for username in followers_username]
            # [get_or_create_profile(username) for username in followers_username]
            # [string for string in a_list if string != ""]
            logging.info(f'Saving followers of \'{username}\'...(({len(followers_usernames)}))')
            create_usernames_batch(followers_usernames)
        except Exception as ex:
            logging.error(f'Exception was thrown when scrolling follow list. Exception: {ex}')
            return
        finally:
            close_btn = browser.find_one("._ac7b._ac7d button._abl-")
            if close_btn is not None:
                close_btn.click()

        statistics_elem = self.browser.find(PROFILE_STATISTICS)

        following_elem = statistics_elem[2]
        following_btn = following_elem
        following_btn.click()

        try:
            following_elems = list(browser.find(PROFILE_FOLLOWERS_ELEMENTS, waittime=0.6) or [])

            if len(following_elems) == 0:
                logging.info(f'Cannot craw profile \'{username}\' because it\'s private.')
                return

            logging.info(f'Scrolling following list...')

            following_elems[-1].location_once_scrolled_into_view
            sleep(2)

            following_elems = list(browser.find(PROFILE_FOLLOWERS_ELEMENTS) or [])
            last_followed = following_elems[-1]
            username_last_check = last_followed.text
            for i in range(40):
                # Scroll down
                self.browser.driver.execute_script(FOLLOWERS_SCROLL_DOWN)
                sleep(2)

                # Get last following
                try:
                    last_followed = browser.find_one(FOLLOWERS_LAST_PROFILE)
                    current_username = last_followed.text
                except AttributeError:
                    break

                # Check if the last username of current iteration is the same as the previous iteration
                if current_username == username_last_check:
                    break

                # Save last username of list
                username_last_check = current_username

            following_elems = list(browser.find(PROFILE_FOLLOWERS_ELEMENTS,
                                                waittime=1) or [])  # avoid assign None to list (which causes an exception)
            followeds_username: List[str] = list([ele.text for ele in following_elems if ele.text != ""])
            # followers: List[Profile] = [get_or_create_profile(username) for username in followers_username]
            # [get_or_create_profile(username) for username in followers_username]
            # [string for string in a_list if string != ""]
            logging.info(f'Saving followings of \'{username}\'...(({len(followeds_username)}))')
            create_usernames_batch(followeds_username)
        except Exception as ex:
            logging.error(f'Exception was thrown when scrolling follow list. Exception: {ex}')
            return
        finally:
            close_btn = browser.find_one("._ac7b._ac7d button._abl-")
            if close_btn is not None:
                close_btn.click()
