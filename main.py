# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import argparse
import logging
from time import sleep
from typing import List

from selenium.common.exceptions import InvalidSessionIdException

from inscrawler import InsCrawler
from inscrawler.api.profile_api import get_profiles_to_crawl
from inscrawler.constants.consts import RETRY_TIMEOUT, MAX_RETRY_TIMEOUT
from inscrawler.settings import override_settings
from inscrawler.settings import prepare_override_settings
from logger import start_logger


def usage():
    return """
        python main.py
        python main.py --debug
    """


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Instagram Crawler - Follow", usage=usage())

    parser.add_argument("--debug", action="store_true")

    prepare_override_settings(parser)

    args = parser.parse_args()

    override_settings(args)

    start_logger()

    ins_crawler = InsCrawler(has_screen=args.debug)
    ins_crawler.login()

    retries = 1
    while True:
        try:
            logging.info("Getting list of profiles to crawl...")
            profiles_to_crawl: List[str] = get_profiles_to_crawl()
            logging.info(f'List of awaiting(to be crawled) profiles returned. - {{{", ".join(profiles_to_crawl)}}}')

            for profile_to_crawl in profiles_to_crawl:
                logging.info(f'Crawling profile : \'{profile_to_crawl}\'...')
                ins_crawler.get_user_profile(profile_to_crawl)
                sleep(10)

            retries = 1
        except InvalidSessionIdException as e:
            logging.exception(e)
            ins_crawler = InsCrawler(has_screen=args.debug)
            ins_crawler.login()
            retries = 1
        except Exception as e:
            logging.exception(e)
            sleep(min(retries * RETRY_TIMEOUT, MAX_RETRY_TIMEOUT))
            retries += 1
